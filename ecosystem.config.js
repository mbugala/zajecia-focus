module.exports = {
  apps : [{
    name: 'front',
    script: './front/node_modules/@angular/cli/bin/ng',
    args: 'serve',
    instances: 1,
    autorestart: true,
    watch: true,
    max_memory_restart: '1G',
    env: {
        "PORT": 4200,
        "NODE_ENV": 'development'
    },
    env_production: {
        "PORT": 5000,
        "NODE_ENV": 'production'
    },
	error_file: '.pm2/front/error/testapp.error',
    out_file: '.pm2/front/log/testapp.log'
  },
  {
    name: 'backend',
    script: './backend/app.js',
    instances: 1,
    autorestart: true,
    watch: true,
    max_memory_restart: '1G',
    env: {
        "PORT": 3000,
        "NODE_ENV": 'development'
    },
    env_production: {
        "PORT": 2000,
        "NODE_ENV": 'production'
    },
    log_date_format: 'YYYY-MM-DD HH:mm:ss.SSS',
    error_file: '.pm2/backend/error/testapp.error',
    out_file: '.pm2/backend/log/testapp.log'
    }],

  deploy : {
  }
};
