# Zajecia-Focus

### PM2

You can use PM2 as a process manager. In order to set it up, first install the PM2 package globally using ```$ npm i -g pm2```.
Upon finishing, you can verify whether the installation was successful with ```$ npx pm2```.
If it displays correct output, you are good to go -- simply type ```$ pm2 start```, 
or ```$ npx pm2 start ecosystem.config.js --env production``` for production,  in the repository's root folder.
You may want to change the location of log and error files in ```ecosystem.config.js```, by default they will be saved in ```./pm2``` directory inside the root folder.

### Ansible

If you wish to deploy apps using Ansible, follow the instruction for PM2, and once you install it, type these commands:

```
$ sudo su
$ apt install ansible
```

Next, download files from ```./ansible``` directory, and run ```$ ansible-playbook playbook.yml -i inventory --connection=local -K``` inside the directory with downloaded files.
You can see if the apps are running using ```$ pm2 status``` command and kill them with ```$ pm2 kill```.
