# Backend

This app is used for real-time communication between clients using Socket.IO.

### Installation

Requires [Node.js](https://nodejs.org/) to run.

Download either the whole monorepo or this directory, then install the dependencies and start the server.

```sh
$ npm install
$ node app.js
```