/* eslint-disable no-console */
const express = require('express');

const app = express();
const { Dialer } = require('dialer');
const cors = require('cors');
const bodyParser = require('body-parser');
const http = require('http').Server(app);
const io = require('socket.io')(http);

const config = {
  url: 'https://uni-call.fcc-online.pl',
  login: 'secret',
  password: 'secret',
};

let messagesCount = 0;

Dialer.configure(config);

app.use(cors());
app.use(bodyParser.json());

http.listen(3000, () => {
  console.log('app listening on port 3000');
});

// Dochodzi komunikacja po websocketach. W połączeniu aplikacji z frontendem websockety umożliwią nam bezpośrednie informowanie frontendu o statusie połączenia. Działanie socket-io jest dobrze opisane pod linkiem: https://socket.io/
io.on('connection', (socket) => {
  console.log('a user connected');
  socket.on('disconnect', () => {
    console.log('a user disconnected');
  });
  socket.on('message', (message) => {
    console.log('message', message);
  });
  io.emit('message', 'connected!');
});

app.post('/call', async (req, res) => {
  const { body } = req;

  console.log(body);

  try {
    const bridge = await Dialer.call(body.number1, body.number2);
    let oldStatus = null;

    // eslint-disable-next-line no-unused-vars
    const interval = setInterval(async () => {
      const currentStatus = await bridge.getStatus();
      if (currentStatus !== oldStatus) {
        oldStatus = currentStatus;
        io.emit('message', currentStatus);
      }
    }, 500);

    res.json({ success: true });
  } catch (err) {
    res.json({ success: false });
    console.error(err);
  }
});

app.post('/chat', async (req, res) => {
  const { body } = req.body;
  io.emit('chat-message', {
    body,
    id: messagesCount,
    sender: req.body.sender,
  });
  messagesCount += 1;
  res.json({ success: true });
});
