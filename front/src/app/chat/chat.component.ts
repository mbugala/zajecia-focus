import { Component, OnInit } from "@angular/core";
import { ChatService } from "../chat.service";
import { Message } from "../message";
import { FormControl } from "@angular/forms";

@Component({
  selector: "app-chat",
  templateUrl: "./chat.component.html",
  styleUrls: ["./chat.component.css"],
  providers: [ChatService],
})
export class ChatComponent implements OnInit {
  newMessage: string;
  messageList: Message[] = [];

  messageFormControl = new FormControl("");

  constructor(private chatService: ChatService) {}

  sendMessage(): void {
    if (this.newMessage.trim().length === 0) {
      this.messageFormControl.setErrors({ required: true });
    } else {
      this.messageFormControl.setErrors({ required: false });

      this.chatService.sendMessage(this.newMessage);
      this.newMessage = "";
    }
  }
  ngOnInit(): void {
    this.chatService.getMessages().subscribe((message: Message) => {
      console.log("got message", message);
      this.messageList.push(message);
      console.log(this.messageList);
    });
  }
}
