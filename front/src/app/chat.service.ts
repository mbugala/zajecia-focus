import { Injectable } from "@angular/core";
import * as io from "socket.io-client";
import { Subject, Observable } from "rxjs";
import { Message } from "./message";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class ChatService {
  private socket = io("localhost:3000");
  private apiUrl = "http://localhost:3000";
  public messageList = new Subject<Message>();

  private getCurrentId = (): string => {
    return this.socket["id"];
  };

  constructor(private http: HttpClient) {
    this.socket.on("chat-message", (msg: Message) => {
      console.log("received chat message", msg);
      if (msg.sender === this.getCurrentId()) {
        msg.sender = "me";
      } else {
        msg.sender = "you";
      }
      this.messageList.next(msg);
    });
  }

  public sendMessage(message): void {
    const clientId = this.getCurrentId();
    const postData = { body: message, sender: clientId };
    console.log("will post", postData);
    this.http.post<Message>(this.apiUrl + "/chat", postData).subscribe();
  }

  public getMessages = (): Observable<Message> => {
    return this.messageList.asObservable();
  };
}
