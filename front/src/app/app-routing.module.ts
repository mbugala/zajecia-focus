import { NgModule } from "@angular/core";
import { WidgetComponent } from "./widget/widget.component";
import { RouterModule, Routes } from "@angular/router";
import { ChatComponent } from "./chat/chat.component";

const routes: Routes = [
  { path: "call", component: WidgetComponent },
  { path: "chat", component: ChatComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
