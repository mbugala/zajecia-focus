import { Component } from "@angular/core";
import { ThemePalette } from "@angular/material/core";
import { Router } from "@angular/router";
import { Location } from "@angular/common";
import { Title } from "@angular/platform-browser";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  title = "front";
  links: string[] = ["call", "chat"];
  activeLink: string = this.links[0];
  background: ThemePalette = undefined;

  capitalize = (s): string => {
    if (typeof s !== "string") return "";
    return s.charAt(0).toUpperCase() + s.slice(1);
  };

  constructor(private titleService: Title, location: Location, router: Router) {
    const currentPath = location.path();
    const link = currentPath.slice(1);
    const correctLink = this.links.find((item) => item === link);
    if (currentPath === "" || correctLink === undefined) {
      router.navigate(["/call"]);
    } else {
      this.activeLink = correctLink;
    }

    router.events.subscribe(() => {
      this.titleService.setTitle(
        `${this.title} | ${this.capitalize(this.activeLink)}`
      );
    });
  }
}
