import { Component } from "@angular/core";
import { CallService } from "../call.service";
import { FormControl, Validators } from "@angular/forms";

@Component({
  selector: "app-widget",
  templateUrl: "./widget.component.html",
  styleUrls: ["./widget.component.css"],
})
export class WidgetComponent {
  number: string;
  validator = /(^[0-9]{9}$)/;
  state = "waiting";
  numberFormControl = new FormControl("", [
    Validators.required,
    Validators.pattern(this.validator),
  ]);

  constructor(private callService: CallService) {}

  call(): void {
    if (this.isValidNumber()) {
      this.callService.placeCall(this.number);
      this.callService.getCallId().subscribe(() => {
        this.checkStatus();
      });
    } else {
      this.numberFormControl.setErrors({ required: true });

      console.info("Numer niepoprawny");
    }
  }

  isValidNumber(): boolean {
    if (!this.validator.test(this.number)) {
      this.numberFormControl.setErrors({ required: true });
    }
    return this.validator.test(this.number);
  }

  checkStatus(): void {
    this.callService.getCallStatus().subscribe((state) => {
      this.state = state;
    });
  }
}
