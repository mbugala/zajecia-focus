export class Message {
  id: string;
  body: string;
  sender: string;
}
