import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Call } from "./call";

import { Subject, Observable } from "rxjs";
import * as io from "socket.io-client";

@Injectable({
  providedIn: "root",
})
export class CallService {
  private apiUrl = "http://localhost:3000";
  private callId = new Subject<number>();
  private callStatus = new Subject<string>();
  private socket = io("http://localhost:3000");

  constructor(private http: HttpClient) {
    this.socket.on("message", (status) => {
      this.callStatus.next(status);
    });
  }

  placeCall(number: string): void {
    const postData = { number1: "512020512", number2: number };
    this.http.post<Call>(this.apiUrl + "/call", postData).subscribe((data) => {
      this.callId.next(data.id);
    });
  }

  getCallId(): Observable<number> {
    return this.callId.asObservable();
  }

  getCallStatus(): Observable<string> {
    return this.callStatus.asObservable();
  }
}
