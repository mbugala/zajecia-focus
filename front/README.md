# Front

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.3.

### Installation

Requires [Node.js](https://nodejs.org/) to run.

Download either the whole monorepo or this directory, then install the dependencies and start the server.

```sh
$ npm install
$ ng serve
```

The application will be running on [localhost:4200](http://localhost:4200) by default, you can access it using a web browser.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
